#!/usr/bin/env bash

./checkout.sh
./update-repositories.sh
./build.sh
./setup.sh