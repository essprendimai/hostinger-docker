#!/usr/bin/env bash

source ./parameters.sh

# Setup base path ------------------------------------------------------------------------------------------------------

if [ ! -d ${PROJECTS_ROOT} ]; then
    mkdir -vp ${PROJECTS_ROOT};
    chmod 777 ${PROJECTS_ROOT};
fi

# Checkout projects ----------------------------------------------------------------------------------------------------

if [ ! -d ${HOSTINGER_BACKEND_DIRECTORY} ]; then
  git clone ${HOSTINGER_BACKEND_GIT} ${HOSTINGER_BACKEND_DIRECTORY};
fi

if [ ! -d ${HOSTINGER_FRONT_DIRECTORY} ]; then
  git clone ${HOSTINGER_FRONT_GIT} ${HOSTINGER_FRONT_DIRECTORY};
fi