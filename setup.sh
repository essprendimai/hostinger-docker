#!/usr/bin/env bash

# Hostinger backend
cp projects/hostinger-backend/.env.example projects/hostinger-backend/.env
docker-compose run --rm hostinger-backend composer install --no-interaction

docker-compose run --rm hostinger-backend php artisan key:generate
docker-compose run --rm hostinger-backend php artisan migrate:fresh
docker-compose run --rm hostinger-backend php artisan module:migrate
docker-compose run --rm hostinger-backend php artisan db:seed
docker-compose run --rm hostinger-backend php artisan module:seed
docker-compose run --rm hostinger-backend php artisan passport:install --force

# Hostinger front
docker-compose run --rm hostinger-front npm install