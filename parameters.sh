#!/usr/bin/env bash

# ----------------------------------------------------------------------------------------------------------------------

# Base paths
PROJECTS_ROOT=projects;

# ----------------------------------------------------------------------------------------------------------------------

# Project paths
HOSTINGER_BACKEND_DIRECTORY=${PROJECTS_ROOT}/hostinger-backend;
HOSTINGER_FRONT_DIRECTORY=${PROJECTS_ROOT}/hostinger-front;

# Project repositories
HOSTINGER_BACKEND_GIT=git@bitbucket.org:essprendimai/hostinger-backend.git;
HOSTINGER_FRONT_GIT=git@bitbucket.org:essprendimai/hostinger-front.git;

# Project containers
HOSTINGER_BACKEND_CONTAINER=hostinger-backend
HOSTINGER_FRONT_CONTAINER=hostinger-front

# ----------------------------------------------------------------------------------------------------------------------